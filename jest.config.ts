
export default {
  testEnvironment:'node',
  moduleFileExtensions: ['ts', 'js', 'json', 'node'],
  clearMocks: true,
  preset: 'ts-jest',
  coveragePathIgnorePatterns: [
    "/node_modules/"
  ],
  detectOpenHandles:true,
  forceExit: true,
  verbose:true,
  coverageProvider: "v8", 
  roots: ['<rootDir>'],
  transform: {
    '^.+\\.ts?$': 'ts-jest'
  },
};
