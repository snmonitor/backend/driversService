//process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
import App from './app';
import {
     ProductController, EmailController,
} from "./controllers";


const port = 1112;

const server = new App([ProductController, EmailController]);

server.start(port, `server available on http://localhost:${port}/api`);

