export interface IProduct {
    IP: string;
    port:string;
    name: string;
    retries:string;
    timeout:string;
    maxOid:string;
    delay:string;
    templateID:string;
    userModified: boolean;
    scannable: boolean;
    zipVersion: string;
}





