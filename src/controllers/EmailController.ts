import express, {Request, Response} from 'express'
import {Controller} from "../Controller";
import { MailgunService} from "../services";
import {HttpException, InternalServerError500Response} from "../responses";

const emailController = express.Router()
const mailgunService = new MailgunService()

emailController.route('/')
    .post(async (req: Request, res: Response) => {
        try {
            const response: HttpException = await mailgunService.send(req.body)
            response.send(res);

        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })

export const EmailController = new Controller('email', emailController);