import { UserRole } from '../enums/UserRole';
import express, {Request, Response} from 'express'
import {Controller} from "../Controller";
import {ProductService as ProductService} from "../services";
import {authorize} from '../middlewares/authMiddleware'
import {HttpException, InternalServerError500Response} from "../responses";

const productController = express.Router()
const productService = new ProductService()

productController.route('/')
    .get(authorize(), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await productService.getProduct(parseInt(req.query.skip?.toString() ?? "0"),
            parseInt(req.query.limit?.toString() ?? "0"));
            response.send(res)

        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .post(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await productService.createProduct(req.body)
            response.send(res);

        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .put(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await productService.updateProduct(req.body);
            response.send(res);
        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    }).delete(authorize([UserRole.admin]), async (req: Request, res: Response) => {
    try {
        const response: HttpException = await productService.deleteProduct(req.body._id)
        response.send(res);
    } catch (e:any) {
        new InternalServerError500Response(e.message).send(res);
    }
})

productController.route('/:id')
    .get(authorize(), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await productService.getProductById(req.params.id);
            response.send(res)

        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .put(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await productService.updateProduct({_id: req.params.id, ...req.body})
            response.send(res);
        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .delete(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await productService.deleteProduct(req.params.id)
            response.send(res);
        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })

productController.route('*')
.delete( async (_req: Request, res: Response) => {

        new InternalServerError500Response().send(res);
})


export const ProductController = new Controller('products', productController);