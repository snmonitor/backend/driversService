
export default {
    "openapi": "3.0.0",
    "info": {
        "version": "0.2.0",
        "title": "KeepMeIn",
        "termsOfService": "http://swagger.io/terms/",
        "contact": {
            "email": "omar.oukil@akka.eu"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "components": {
            securitySchema: {
                bearerAuth: {
                    type: "http",
                    schema: "bearer",
                    bearerFormat:"JWT"
                }
            }
        },
        security: [
            {
                bearerAuth:[],
            }
        ]
    },
    "host": "localhost:1111/api",
    "basePath": "/",
    "tags": [
        {
            "name": "user",
            "description": "Operations about user"
        }
    ],
    "schemes": [
        "http",
    ],
    "paths": {
        "/users": {
            "post": {
                "tags": [
                    "user"
                ],
                "summary": "Create user",
                "operationId": "createUser",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "in": "body",
                        "name": "body",
                        "description": "Created user body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                ],
                "responses": {
                    "201": {
                        "description": "successful operation",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    },
                }
            },
            "get": {
                "tags": [
                    "user"
                ],
                "summary": "get all user",
                "operationId": "getUser",
                "produces": [
                    "application/json"
                ],

                "responses": {
                    "400": {
                        "description": "Invalid user supplied"
                    },
                    "404": {
                        "description": "User not found"
                    }
                }
            },
            "put": {
                "tags": [
                    "user"
                ],
                "summary": "Updated user",
                "operationId": "updateUser",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "in": "body",
                        "name": "body",
                        "description": "Updated user object",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                ],
                "responses": {
                    "400": {
                        "description": "Invalid user supplied"
                    },
                    "404": {
                        "description": "User not found"
                    }
                }
            },

        },
        "/users/{_id}": {
            "get": {
                "tags": [
                    "user"
                ],
                "summary": "Get user by id",
                "operationId": "getUserById",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "_id",
                        "in": "path",
                        "description": "The id that needs to be fetched",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    },
                    "400": {
                        "description": "Invalid id supplied"
                    },
                    "404": {
                        "description": "User not found"
                    }
                }
            },
            "put": {
                "tags": [
                    "user"
                ],
                "summary": "Updated user",
                "operationId": "updateUserById",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "_id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "in": "body",
                        "name": "body",
                        "description": "Updated user object",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                ],
                "responses": {
                    "400": {
                        "description": "Invalid user supplied"
                    },
                    "404": {
                        "description": "User not found"
                    }
                }
            },
            "delete": {
                "tags": [
                    "user"
                ],
                "summary": "Delete user",
                "operationId": "deleteUser",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "_id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "400": {
                        "description": "{\"message\": \"user deleted\",\"data\": {\"_id\":\"62456d6a1c3edb7528d7a12c\"}}"
                    },
                    "404": {
                        "description": "User not found"
                    }
                }
            }
        }
    },

    "definitions": {
        "User": {
            "type": "object",
            "required": [
                "name",
                "mdp",
                "email",
                "accesToken",
                "role",
                "_id"
            ],
            "properties": {
                "_id": {
                    "type": "string",
                    "default": ""
                },
                "name": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "mdp": {
                    "type": "string",
                    "format": "password"
                },
                "accesToken": {
                    "type": "string"
                },
                "role": {
                    "type": "string"
                },
                "emailVerified": {
                    "type": "boolean",
                    "default": "false"
                },
                "status": {
                    "type": "string",
                    "default": "new"
                }
            }
        }
    }
}