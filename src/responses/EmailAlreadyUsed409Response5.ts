import {HttpException} from "./index";

export class EmailAlreadyUsed409Response extends HttpException {
    constructor(email: string) {
        super([email],5, 409, `The email <${email}> is already Used`);
    }
}