import {HttpException} from "./index";

export class IPHostNotFound400Response extends HttpException {
    constructor(hostname="localhost",port="161") {
        super([hostname,port],21, 400, `IP host not found : ${hostname} ; port : ${port}`);
    }
}