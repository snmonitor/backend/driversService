import {HttpException} from "./index";

export class BadRequest400Response extends HttpException {
    constructor(message="bad request") {
        super([],2, 400, message);
    }
}