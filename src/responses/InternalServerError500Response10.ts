import {HttpException} from "./index";

export class InternalServerError500Response extends HttpException {
    constructor(message: any = `The server has encountered an error`) {
        super([],10, 500, message);
    }
}

