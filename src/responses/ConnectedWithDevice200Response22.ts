import {HttpException} from "./index";

export class ConnectedWithDevice200Response extends HttpException {
    constructor(data = {},productId:string) {
        super([],22, 200,"Connected with device", {productId:productId,kpis:data});
    }
}