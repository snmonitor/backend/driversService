import {HttpException} from "./index";

export class Found200Response extends HttpException {
    constructor(str: string = '', leng: number = 0, data: object = {},pagination:object={}) {
        super([leng,str],8, 200, `got ${leng} ${str}`, data,pagination);
    }
}