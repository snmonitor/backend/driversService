import {HttpException} from "./index";

export class Authenticated200Response extends HttpException {
    constructor(tokenLife:string,data = {}) {
        super([tokenLife],1, 200, `Authenticated token life time : ${tokenLife}`, data);
    }
}