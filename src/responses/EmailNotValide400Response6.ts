import {HttpException} from "./index";

export class EmailNotValide400Response extends HttpException {
    constructor(email: string) {
        super([email],6, 400, `The email <${email}> is not valide`);
    }
}