import {Router} from "express";


export class Controller {
    private _name: string;
    private _controller: Router;

    constructor(name: string, controller: Router) {
        this._name = name;
        this._controller = controller;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get controller(): Router {
        return this._controller;
    }

    set controller(value: Router) {
        this._controller = value;
    }
}