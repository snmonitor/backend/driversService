import * as express from "express";
import bodyParser from "body-parser";
import cors from 'cors'
import fs from 'fs-extra'
import morgan, { StreamOptions } from "morgan";
import Instance from './instance'
import { Controller } from "./Controller";
import swaggerUi from 'swagger-ui-express';
import swaggerDoc from './swagger';
import http from "http";
import moment from "moment";
export default class App {

    private _app: express.Application
    get app(): express.Application {
        return this._app
    }

    constructor(controllers: Controller[]) {
        this._app = express.default()

        if (process.env.NODE_ENV !== "test")
            this.initializingLogger();
        this.initializingMiddleware();
        this.initializingController(controllers);
        this._app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
    }

    initializingMiddleware() {
        this._app.use(bodyParser.urlencoded({ extended: false, limit: '5mb' }));
        this._app.use(bodyParser.json({ limit: '5mb' }));
        this._app.use(cors());
    }

    initializingController(controllers: Controller[]) {
        controllers.forEach((e) => {
            this._app.use(`/api/${e.name}`, e.controller,);
        })
    }

    start(port: number, message: string) {

        const httpSocket = http.createServer(this.app)

        httpSocket.listen(port, () => {
            Instance.get().logger.info(message)
            Instance.get().logger.info('swagger available at /api/docs ')
        })
        process.on('SIGINT', () => {
            Instance.get().logger.info('process exit')
            process.exit(0);
        });

    }

    dateGenerator() {

        return `${moment().toISOString().split("T")[0]}-${moment().toISOString().split("T")[1].split(':')[1]}.log`;
    }

    initializingLogger() {
        // checking if the folder log exist
        if (!fs.existsSync('./log')) {
            fs.mkdirSync('./log');
        }
        const stream: StreamOptions = {
            write: (message: any) =>
                Instance.get().logger.http(message),
        };
        this._app.use(morgan(':method/:status :url, :total-time[3]ms', { stream }))
    }
}



