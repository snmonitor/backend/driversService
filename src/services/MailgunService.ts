import Instance from "../instance";
import { HttpException,Success200Response } from "../responses";
import config from "config"

export class MailgunService{

     async send(body:any):Promise<HttpException>
     {
          const data={from:body.from,
               to:body.to,
               subject:body.subject,
               text:body.text}

          const message=await Instance.get().mailgun.messages.create(await config.get('MAILGUN_DOMAIN'),data);
               return new Success200Response(message)
}
}
