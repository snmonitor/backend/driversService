import { ProductRepository as ProductRepository } from "../repositories";
import { isValidObjectId } from "mongoose";

import {
    Created201Response,
    Deleted200Response,
    FieldMissing400Response,
    Found200Response,
    HttpException,
    NotFound404Response,
    NotUUID404Response,
    Updated200Response
} from "../responses";
import { isNullEmptyOrWhiteSpace } from "../helper";


export class ProductService {

    private productRepository = new ProductRepository();

    async getProductById(id: string): Promise<HttpException> {
        if (isValidObjectId(id)) {
            const product = await this.productRepository.getProductById(id);
            if (product !== null) {
                return new Found200Response('product', 1, product);
            } else {
                return new NotFound404Response(id, 'product');
            }
        } else
            return new NotUUID404Response();
    }


      

    async getProduct(skip = 0, limit = 0): Promise<HttpException> {
        const data = await this.productRepository.getProduct(skip, limit)
        return new Found200Response('product', data.products.length ?? 0, data.products, data.pagination)
    }

   
    async createProduct(data: { [key: string]: any }): Promise<HttpException> {
        // all required field check
        const check = this.checkRequired(data)
        if (check === 'true') {
            const product = await this.productRepository.createProduct(data);
            return new Created201Response('product', product);
        } else
            return new FieldMissing400Response(check)
    }

    async updateProduct(data: { [key: string]: any }): Promise<HttpException> {
        if (data._id) {
            if (isValidObjectId(data._id)) {
                const res: object = await this.productRepository.patchProduct(data);
                if (res !== null)
                    return new Updated200Response('product', data)
                else
                    return new NotFound404Response(data._id, 'product');
            } else
                return new NotUUID404Response();
        } else
            return new FieldMissing400Response();
    }

    async deleteProduct(id: string): Promise<HttpException> {
        if (id) {
            if (isValidObjectId(id)) {
                
                const res: object = await this.productRepository.deleteProduct(id);
                if (res !== null)
                    return new Deleted200Response('product', { _id: id })
                else
                    return new NotFound404Response(id, 'product');
            } else
                return new NotUUID404Response();
        } else
            return new FieldMissing400Response()
    }

    public checkRequired(json: { [key: string]: any }): string {
        if (isNullEmptyOrWhiteSpace(json.name))
            return 'name'
        
        return 'true'
    }



}