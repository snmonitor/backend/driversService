import {ProductModel as ProductModel} from '../schemas'

export class ProductRepository {

    private productModel = ProductModel;

    async getProductById(id: string): Promise<any> {
        return this.productModel.findById(id)
    }

    async productExist(id: string) {
        return this.productModel.findById(id).select('_id');
    }

    //get with pagination
    async getProduct(skip=0,limit=0): Promise<any> {
        const products= await this.productModel.find().skip(skip).limit(limit);
        const tradesCollectionCount = await this.productModel.count()
        const totalPages=limit===0?1: Math.ceil(tradesCollectionCount / limit)
        const currentPage=skip===0?1: Math.ceil(((skip+1)/limit))
        return {pagination: {
            total: tradesCollectionCount,
            currentpage: currentPage===0?1:currentPage,
            pages: totalPages,
          },products: products}
        
    }

    async createProduct(data: { [key: string]: any }): Promise<object> {
        const product = new this.productModel(data);
        await product.save();
        return product
    }

    async patchProduct(data: { [key: string]: any }): Promise<object> {
        return this.productModel.findOneAndUpdate({_id: data._id}, data) as object;
    }

    async deleteProduct(id: string): Promise<object> {
        return this.productModel.findOneAndDelete({_id: id}) as object;
    }


}