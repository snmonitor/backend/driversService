import {model, Schema} from "mongoose";
import {IProduct} from "../interfaces";

export const ProductSchema: Schema<IProduct> = new Schema<IProduct>(
    {
        name: {type: String, required: true},
        userModified: Boolean,
        scannable: Boolean,
        zipVersion: String,
    }, {timestamps: true}
);

export const ProductModel = model('Product', ProductSchema, 'Product')