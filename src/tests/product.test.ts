import Request from './Request';
import mongoose from 'mongoose';
import config from 'config';
import { product } from './mocks/productMock';

const path = "/api/products";


const request = new Request()
let id = ''



afterAll(async () => {
     await mongoose.disconnect()
     await mongoose.connection.close()
})

beforeAll(async () => {
     await request.getToken()
     const url: string = await config.get("CONNECTION_STRING");
     await mongoose.connect(url)
})



describe("GET ALL PRODUCTS", () => {
     test("found", async () => {
          const res = await request.GET(path);
          expect(res.body.code).toBe(8)
     })
})

describe("CREATE PRODUCT", () => {
     test("product Created", async () => {
          const res = await request.POST(`${path}`, product);
          id = res.body?.donne?._id
          expect(res.body.code).toBe(3)
     })

     test("missing field", async () => {
          const res = await request.POST(`${path}`, {});
          expect(res.body.code).toBe(7)
     })
})
describe("GET PRODUCT BY ID", () => {
     test("found", async () => {
          const res = await request.GET(`${path}/${id}`);
          expect(res.body.code).toBe(8)
     })
     test("the id is not a uuid", async () => {
          const res = await request.GET(`${path}/ddwdw`);
          expect(res.body.code).toBe(14)
     })

     test("not found", async () => {
          const res = await request.GET(`${path}/62728b2ccc1488cb23359127`);
          expect(res.body.code).toBe(12)
     })
})

describe("DELETE PRODUCT", () => {

     test("id provided not uuid", async () => {
          const res = await request.DELETE(`${path}/ffff`)

          expect(res.body.code).toBe(14)
     })

})

